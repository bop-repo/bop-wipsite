from flask import Flask, render_template

app = Flask(__name__)


@app.errorhandler(404)
def error_404(_err):
    return render_template("404.html")


@app.route("/")
def index():
    return render_template("index.html")


if __name__ == "__main__":
    app.run()
